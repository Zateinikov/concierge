<?php
namespace ConciergeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuildingType extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder -> add('name',null, array(
            'attr' => array('autofocus' => true),
            'label' => 'label.name'
        ));
    }
    
    public function configureOptions(OptionsResolver $resolver) 
    {
        $resolver->setDefaults(array(
            'data_class' => 'ConciergeBundle\Entity\Building'
        ));
    }
}

