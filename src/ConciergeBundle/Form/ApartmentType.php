<?php
namespace ConciergeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType; 

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ApartmentType extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder -> add('number', null, array(
            'attr' => array('autofocus' => true),
            'label' => 'label.number'
        ));
        
        $builder->add('users', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', array(
            'entry_type' => 'ConciergeBundle\Form\UserType',
        ));
        
//        $builder->add('usersl', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
//            'class' => 'ConciergeBundle:User',
//            'choice_label' => 'username',
//        ));
    }
    
    public function configureOptions( OptionsResolver $resolver) 
    {
        $resolver->setDefaults(array(
            'data_class' => 'ConciergeBundle\Entity\Apartment'
        ));
    }
    
}