<?php

namespace ConciergeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\CallbackTransformer;

class GroupType extends AbstractType
{
    public function buildForm(Formbuilderinterface $builder, array $options)
    {
        $builder->add('name');
        
        $builder->add(
                $builder->create('roles', null )
                    ->addModelTransformer(new CallbackTransformer(
                            function ($originalDescription) {
                                return $originalDescription;
                            },
                            function ($submittedDescription) {
                                return $submittedDescription;
                            }
                            ))
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ConciergeBundle\Entity\Group'
        ));
    }
}
