<?php
namespace ConciergeBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ConciergeBundle\Entity\Building;
use ConciergeBundle\Entity\Apartment;

/**
 * @Route("/admin/building")
 * @Security("has_role('ROLE_ADMIN')")
 */
class BuildingController extends Controller {
    
    /**
     * @Route("/", name="admin_building_index")
     */
    public function indexAction(){
        $buildings =$this->getDoctrine()->getManager()->getRepository('ConciergeBundle:Building')->findAll();
        return $this->render('admin/building/index.html.twig', array('buildings' => $buildings));
    }
    
    /**
     * @Route("/new", name="admin_building_new")
     */
    public function newAction(Request $request){
        $building = new Building();
        
        $form = $this->createForm('ConciergeBundle\Form\BuildingType', $building);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($building);
            $em->flush();
            
            $this->addFlash('success', 'building.created_success');
            
            return $this->redirectToRoute('admin_building_index');
        }
        
        return $this->render('admin/building/new.html.twig', array(
            'building' => $building,
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/{id}/edit", requirements={"id" = "\d+"}, name="admin_building_edit" )
     */
    public function editAction(Building $building, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $form_edit = $this->createForm('ConciergeBundle\Form\BuildingType', $building);
        $form_delete = $this->createDeleteForm($building);
        $form_edit->handleRequest($request);
        if($form_edit->isSubmitted() && $form_edit->isValid())
        {
            $em->flush();
            
            $this->addFlash('success', 'building.edit_successfully');
            return $this->redirectToRoute('admin_building_index');
        }
       
        return $this->render('\admin\building\edit.html.twig', array(
            'building' => $building,
            'form_edit' => $form_edit->createView(),
            'form_delete' => $form_delete->createView(),
        ));
    }
    
    /**
     * @Route("/{id}", requirements = {"id" = "\d+"}, name="admin_building_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Building $building)
    {
        return $this->render('\admin\building\show.html.twig', array(
            'building' => $building
        ));
    }
    
    /**
     * @Route("/{id}", requirements = {"id" = "\d+"}, name="admin_building_delete")
     * @Method({"DELETE"})
     */
    public function deleteAction(Building $building, Request $request)
    {
        $form = $this->createDeleteForm($building);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($building);
            $entityManager->flush();

            $this->addFlash('success', 'building.deleted_successfully');
        }

        return $this->redirectToRoute('admin_building_index');
    }
    
    /**
     * 
     * @param Building $building object
     * @return Symfony\Component\Form\Form
     */
    private function createDeleteForm(Building $building)
    {
         return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_building_delete', array('id' => $building->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    private function createEntites_(){
        $em = $this->getDoctrine()->getManager();
        $building = new Building();
        $building->setName('Liko'.rand(1,10));

        $ap_cnt = rand(2,10);
        for ($i=1; $i<$ap_cnt; $i++){
            $ap = new Apartment();
            $ap->setNumber($i);
            $ap->setBuilding($building);

            $em->persist($building);
            $em->persist($ap);
            $em->flush();
        }
    }
}
