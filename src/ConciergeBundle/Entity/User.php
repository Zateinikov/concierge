<?php

namespace ConciergeBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ConciergeBundle\Entity\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToMany(targetEntity="Apartment", inversedBy="users")
     * @ORM\JoinTable(name="users_apartments")
     */
    private $apartments;
    
    /**
     * @ORM\ManyToMany(targetEntity="ConciergeBundle\Entity\Group")
     * @ORM\JoinTable(name="fos_user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Add apartments
     *
     * @param \ConciergeBundle\Entity\Apartment $apartments
     * @return User
     */
    public function addApartment(\ConciergeBundle\Entity\Apartment $apartments)
    {
        $this->apartments[] = $apartments;

        return $this;
    }

    /**
     * Remove apartments
     *
     * @param \ConciergeBundle\Entity\Apartment $apartments
     */
    public function removeApartment(\ConciergeBundle\Entity\Apartment $apartments)
    {
        $this->apartments->removeElement($apartments);
    }

    /**
     * Get apartments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApartments()
    {
        return $this->apartments;
    }
}
