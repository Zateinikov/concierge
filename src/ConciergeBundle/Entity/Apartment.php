<?php
namespace ConciergeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="apartment")
 */
class Apartment {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=10)
     */
    private $number;
    
    /**
     * @ORM\ManyToOne(targetEntity="Building", inversedBy="apartments")
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id")
     */
    private $building;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="apartments")
     */
    private $users;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Apartment
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set building
     *
     * @param \ConciergeBundle\Entity\Building $building
     * @return Apartment
     */
    public function setBuilding(\ConciergeBundle\Entity\Building $building = null)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return \ConciergeBundle\Entity\Building 
     */
    public function getBuilding()
    {
        return $this->building;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add users
     *
     * @param \ConciergeBundle\Entity\User $users
     * @return Apartment
     */
    public function addUser(\ConciergeBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \ConciergeBundle\Entity\User $users
     */
    public function removeUser(\ConciergeBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
}
