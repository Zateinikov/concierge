<?php

namespace ConciergeBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminController extends Controller
{

    /**
     * @Route("/", name="admin_index")
     * 
     */
    public function indexAction()
    {
        //return $this->render('ConciergeBundle:Default:index.html.twig');
        
        // replace this example code with whatever you need
        return $this->render('admin/index.html.twig');
    }

}
