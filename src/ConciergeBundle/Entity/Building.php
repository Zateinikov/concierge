<?php
namespace ConciergeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ConciergeBundle\Entity\Apartment;

/**
 * @ORM\Entity
 * @ORM\Table(name="building")
 */
class Building {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=60)
     */
    private $name;
    
    /**
     * @ORM\OneToMany(targetEntity="Apartment", mappedBy="building")
     */
    private $apartments;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Building
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apartments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add Apartments
     *
     * @param \ConciergeBundle\Entity\Apartment $apartments
     * @return Building
     */
    public function addApartment(\ConciergeBundle\Entity\Apartment $apartments)
    {
        $this->apartments[] = $apartments;

        return $this;
    }

    /**
     * Remove Apartments
     *
     * @param \ConciergeBundle\Entity\Apartment $apartments
     */
    public function removeApartment(\ConciergeBundle\Entity\Apartment $apartments)
    {
        $this->apartments->removeElement($apartments);
    }

    /**
     * Get Apartments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApartments()
    {
        return $this->apartments;
    }
}
