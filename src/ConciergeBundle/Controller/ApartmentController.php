<?php
namespace ConciergeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use ConciergeBundle\Entity\Building;
use ConciergeBundle\Entity\Apartment;
use ConciergeBundle\Form\ApartmentType;

/**
 * @Route("/apartment")
 */
class ApartmentController extends Controller {
    /**
     * @Route("/{id}", requirements = {"id" = "\d+"}, name = "admin_apartment_show")
     * @Method({"POST", "GET"})
     * @param Request $request
     */
    public function snowAction(Apartment $apartment)
    {
        return $this->render('\admin\apartment\show.html.twig', array(
            'apartment' => $apartment
        ));
    }
    
    /**
     * @Route("/{id}/edit", requirements = {"id" = "\d+"}, name = "admin_apartment_edit")
     */
    public function editAction(Apartment $apartment, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $form_edit = $this->createForm('ConciergeBundle\Form\ApartmentType', $apartment);
        $form_edit->handleRequest($request);
        
        
        if($form_edit->isSubmitted() && $form_edit->isValid())
        {
            $em->flush();
            
            return $this->redirectToRoute('admin_apartment_show', array("id" => $apartment->getId()));
        }
        
        $form_delete = $this->createDeleteForm($apartment);
        
        return $this->render('\admin\apartment\edit.html.twig', array(
            'apartment' => $apartment,
            'form_edit' => $form_edit->createView(),
            'form_delete' => $form_delete->createView(),
        ));
    }
    
    /**
     * @Route("/{building_id}/new", name="apartment_new")
     * @ParamConverter("building", options={"mapping": {"building_id": "id"}})
     
     * @Method("POST")
     *
     * NOTE: The ParamConverter mapping is required because the route parameter
     * (postSlug) doesn't match any of the Doctrine entity properties (slug).
     * See http://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html#doctrine-converter
     */
    public function apartmentNewAction(Request $request, Building $building)
    {
        $form = $this->createForm('ConciergeBundle\Form\ApartmentType');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Apartment $apt */
            $apt = $form->getData();
            $apt->setBuilding($building);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($apt);
            $entityManager->flush();

            return $this->redirectToRoute('admin_building_show', array('id' => $building->getId()));
        }

        return $this->render('blog/comment_form_error.html.twig', array(
            'post' => $post,
            'form' => $form->createView(),
        ));
    }
    
    public function formAction(Building $building)
    {

        $form = $this->createForm('ConciergeBundle\Form\ApartmentType');
        
        return $this->render('admin/apartment/_add_form.html.twig', array(
            'building' => $building,
            'form' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/{id}", requirements = {"id" = "\d+"}, name="admin_apartment_delete")
     * @Method({"DELETE"})
     */
    public function deleteAction(Apartment $apartment, Request $request)
    {
        $form = $this->createDeleteForm($apartment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($apartment);
            $entityManager->flush();

            $this->addFlash('success', 'apartment.deleted_successfully');
        }

        return $this->redirectToRoute('admin_building_show', array('id' => $apartment->getBuilding()->getId()));
    }
    
    /**
     * 
     * @param Apartment $apartment object
     * @return Symfony\Component\Form\Form
     */
    private function createDeleteForm(Apartment $apartment)
    {
         return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_apartment_delete', array('id' => $apartment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
